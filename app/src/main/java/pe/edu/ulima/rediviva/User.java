package pe.edu.ulima.rediviva;

import io.realm.RealmObject;

/**
 * Created by Christian Llanos on 10/3/2017.
 */

public class User extends RealmObject {
    private String name, lastName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
